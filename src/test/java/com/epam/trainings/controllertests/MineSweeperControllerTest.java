package com.epam.trainings.controllertests;

import com.epam.trainings.mvc.controller.minesweepercontroller.MineSweeperControllerImpl;
import com.epam.trainings.mvc.model.minesweeper.MineSweeperModel;
import com.epam.trainings.mvc.model.minesweeper.entry.Entry;
import com.epam.trainings.mvc.view.minesweeperview.MineSweeperView;
import org.junit.Rule;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collection;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(Parameterized.class)
public class MineSweeperControllerTest {


  @Rule
  public MockitoRule rule = MockitoJUnit.rule();
  private static final String[][] print = {{""}};

  @Mock Entry entry;

  @Mock MineSweeperView view;

  @Mock MineSweeperModel model;

  @InjectMocks MineSweeperControllerImpl controller = spy(new MineSweeperControllerImpl(model, view));

@ParameterizedTest
@MethodSource("getTestEntries")
  public void playTest(Entry[][] entries) {
    lenient().when(entry.isOpened()).thenReturn(false);
    when(model.getScreenVersion()).thenReturn(print);
    when(model.getEntries()).thenReturn(entries);
    when(model.getSafe_fields_num()).thenReturn(1);
    assertFalse(controller.play(5, 5, 99));
  }

  @Parameterized.Parameters
  public static Collection getTestEntries() {
    return Arrays.asList(
        new Object[][] {
          {new Entry[][] {{new Entry(true), new Entry(false)}, {new Entry(true), new Entry(false)}}}
        });
  }
}
