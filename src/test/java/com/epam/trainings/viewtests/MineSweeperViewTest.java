package com.epam.trainings.viewtests;

import com.epam.trainings.mvc.controller.minesweepercontroller.MineSweeperController;
import com.epam.trainings.mvc.view.minesweeperview.MineSweeperView;
import com.epam.trainings.mvc.view.minesweeperview.MineSweeperViewImpl;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import static org.mockito.Mockito.*;

public class MineSweeperViewTest {

    @Rule
    MockitoRule rule = MockitoJUnit.rule();

    MineSweeperView view = new MineSweeperViewImpl();

    @Mock
    MineSweeperController controller;

    @Test
    public void startPlayTest(){
       when(controller.play(5,5,5)).thenReturn(false);
    }
}
