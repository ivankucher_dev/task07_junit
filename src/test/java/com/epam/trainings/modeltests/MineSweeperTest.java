package com.epam.trainings.modeltests;

import com.epam.trainings.mvc.model.minesweeper.MineSweeper;
import com.epam.trainings.mvc.model.minesweeper.entry.Entry;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;


import static org.junit.jupiter.api.Assertions.*;


@RunWith(Parameterized.class)
public class MineSweeperTest {
    public MineSweeper mineSweeper;


    @ParameterizedTest
    @MethodSource("getTestEntries")
    public void testNeighboringBombs(Entry[][] entries){
        mineSweeper = new MineSweeper();
       mineSweeper.setupMineSweeperModel(2,2,5);
        mineSweeper.setEntries(entries);
        assertEquals(mineSweeper.countNeighboardingBombs(0,1),2);

    }

    @ParameterizedTest
    @MethodSource("getTestEntries")
    public void testOpenField(Entry[][] entries){
        mineSweeper = new MineSweeper();
        mineSweeper.setupMineSweeperModel(2,2,5);
        mineSweeper.setEntries(entries);
        assertFalse(mineSweeper.openField(0,0));
    }

    @Parameterized.Parameters
    public static Collection getTestEntries(){
        return Arrays.asList(new Object[][]{
                {
                    new Entry[][]{{new Entry(true),new Entry(false)},{new Entry(true),new Entry(false)}}
                }
        });
    }
}
