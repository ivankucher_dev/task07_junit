package com.epam.trainings.modeltests;

import com.epam.trainings.mvc.model.lingestplateau.Plateau;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class PlateauTest {

    private Plateau plateau;

    @ParameterizedTest
    @MethodSource("getArray")
    public void testLongestPlateau(int [] array){
       plateau = new Plateau();
       plateau.initArray(array);
       assertEquals(Integer.valueOf(plateau.longestPlateau()),3);

    }

    static Collection getArray(){
        return Arrays.asList( new int[] {1,2,3,4,4,4,5,5,5,6,5,6});
    }
}
