package com.epam.trainings.mvc.controller.viewcontroller;

import com.epam.trainings.mvc.controller.longestplateaucontroller.LongestPlateauController;
import com.epam.trainings.mvc.controller.longestplateaucontroller.LongestPlateauControllerImpl;
import com.epam.trainings.mvc.controller.minesweepercontroller.MineSweeperController;
import com.epam.trainings.mvc.controller.minesweepercontroller.MineSweeperControllerImpl;
import com.epam.trainings.mvc.model.lingestplateau.Plateau;
import com.epam.trainings.mvc.model.minesweeper.MineSweeper;
import com.epam.trainings.mvc.view.longestplateauview.LongestPlateauView;
import com.epam.trainings.mvc.view.mainview.View;
import com.epam.trainings.mvc.view.minesweeperview.MineSweeperViewImpl;

public class ViewControllerImpl implements ViewController {
    private View view;

    public ViewControllerImpl(View view) {
        this.view = view;
    }

    public void playMineSweeperGame(){
        MineSweeper mineSweeper = new MineSweeper();
        MineSweeperViewImpl mineSweeperView = new MineSweeperViewImpl();
        MineSweeperController mineSweeperController = new MineSweeperControllerImpl(mineSweeper,mineSweeperView);
        mineSweeperView.setController(mineSweeperController);
    }

    public void playLongestPlateauGame(){

        Plateau plateau = new Plateau();
        LongestPlateauView view = new LongestPlateauView();
        LongestPlateauController longestPlateauController = new LongestPlateauControllerImpl(view,plateau);
        view.setController(longestPlateauController);
    }
}
