package com.epam.trainings.mvc.controller.viewcontroller;

public interface ViewController {
  void playMineSweeperGame();

  void playLongestPlateauGame();
}
