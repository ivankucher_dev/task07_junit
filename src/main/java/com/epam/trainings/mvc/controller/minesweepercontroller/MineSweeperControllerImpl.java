package com.epam.trainings.mvc.controller.minesweepercontroller;

import com.epam.trainings.mvc.model.minesweeper.MineSweeperModel;
import com.epam.trainings.mvc.view.minesweeperview.MineSweeperView;
import static com.epam.trainings.utils.PropertiesGetter.*;
import java.util.Arrays;

public class MineSweeperControllerImpl implements MineSweeperController {
  private MineSweeperModel mineSweeper;
  private MineSweeperView view;

  public MineSweeperControllerImpl(MineSweeperModel mineSweeper, MineSweeperView view) {
    this.mineSweeper = mineSweeper;
    this.view = view;
  }

  public boolean play(int width, int height, int probability) {
    boolean inGame = true;
    setupMineSweeper(width, height, probability);
    while (inGame) {
      view.paintResult("Choose one :");
      view.paintResult(getBoardToShow());
      inGame = openBoardField(width, height);
    }
    if (mineSweeper.getSafe_fields_num() == 0) {
      view.paintResult(getProperties().getProperty("winMessage"));
      return true;
    } else {
      view.paintResult(getBoardToShow());
      view.paintResult(getProperties().getProperty("loseMessage"));
      return false;
    }
  }

  private boolean openBoardField(int width, int height) {
    int[] field;
    do {
      field = readField(width - 1, height - 1);
    } while (mineSweeper.getEntries()[field[0]][field[1]].isOpened());

    boolean inGame = mineSweeper.openField(field[0], field[1]);
    return inGame;
  }

  private int[] readField(int width, int height) {
    int rowNum;
    int columnNum;
    do {
      view.paintResult("Input not opened field by number on the screen : ");
      columnNum = view.readInput();
      rowNum = view.readInput();

    } while (columnNum > height || rowNum > width);
    return new int[] {columnNum, rowNum};
  }

  private void setupMineSweeper(int width, int height, int probability) {
    mineSweeper.setupMineSweeperModel(width, height, probability);
  }

  public String getBoardToShow() {
    StringBuilder sb = new StringBuilder();
    Arrays.stream(mineSweeper.getScreenVersion())
        .map(a -> String.join(" ", a))
        .forEach(a -> sb.append(a + "\n"));
    return sb.toString();
  }
}
