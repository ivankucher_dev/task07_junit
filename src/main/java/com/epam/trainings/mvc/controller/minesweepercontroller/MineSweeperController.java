package com.epam.trainings.mvc.controller.minesweepercontroller;

public interface MineSweeperController {
  boolean play(int width, int height, int probability);

  String getBoardToShow();
}
