package com.epam.trainings.mvc.controller.longestplateaucontroller;

public interface LongestPlateauController {
  void getLongestPlateau(int[] array);

  int[] createArray(boolean automatically);
}
