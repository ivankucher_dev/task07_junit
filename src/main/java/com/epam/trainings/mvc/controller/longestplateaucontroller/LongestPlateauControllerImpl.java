package com.epam.trainings.mvc.controller.longestplateaucontroller;

import com.epam.trainings.mvc.model.lingestplateau.Plateau;
import com.epam.trainings.mvc.view.longestplateauview.LongestPlateauView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Random;

public class LongestPlateauControllerImpl implements LongestPlateauController {
    private static final int MAX_LENGTH = 30;
    private static final int MIN_LENGTH = 1;
    private static final int MAX_RANDOM_LIMIT = 99;
    private static final int MIN_RANDOM_LIMIT = 0;
    private static Logger log = LogManager.getLogger(LongestPlateauControllerImpl.class.getName());
    private LongestPlateauView view;
    private Plateau plateau;
    private Random r;

    public LongestPlateauControllerImpl(LongestPlateauView view, Plateau plateau)  {
        this.view = view;
        this.plateau = plateau;
        r = new Random();
    }

    public void getLongestPlateau(int [] array){
        plateau.initArray(array);
       String result =  plateau.longestPlateau();
       view.showResult(result);
    }

    public int[] createArray(boolean automatically){
        if(automatically){
          return  generateAutomatically();
        }
        int [] array;
        int length = readLengthOfArray();
        array = new int[length];
    log.info("Enter array numbers : ");
        for(int i=0;i<length;i++){
            array[i] = view.readInput();
        }
        return array;
    }

    private int [] generateAutomatically(){
        int i=0;
        int [] array;
        int length = readLengthOfArray();
        array = new int[length];
    while (i!=length-1) {
      array[i] = r.nextInt(MAX_RANDOM_LIMIT - MIN_RANDOM_LIMIT) + MIN_RANDOM_LIMIT;
      i++;
        }
    return array;
    }

    private int readLengthOfArray(){
        int length;
        do{
            log.info("Enter length of array (from 1 to 30) : ");
            length = view.readInput();
        } while (length>MAX_LENGTH || length<MIN_LENGTH);
        return length;
    }
}
