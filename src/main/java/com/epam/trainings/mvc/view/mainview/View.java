package com.epam.trainings.mvc.view.mainview;

import com.epam.trainings.mvc.controller.viewcontroller.ViewController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static com.epam.trainings.utils.PropertiesGetter.*;

import java.util.Scanner;

public class View {
  private ViewController controller;
  private Scanner sc;
  private static final int LAST_GAME = 2;
  private static final int FIRST_GAME = 1;
  private static final int PLATEAU = 2;
  private static final int MINE_SWEEPER = 1;
  private static Logger log = LogManager.getLogger(View.class.getName());

  public void setController(ViewController controller) {
    this.controller = controller;
    sc = new Scanner(System.in);
    do {
      play();
    } while (stop());
  }

  private void play() {
    log.info("What do you want to play ?");
    log.info(getProperties().getProperty("gameList"));
    switch (readGame()) {
      case MINE_SWEEPER:
        controller.playMineSweeperGame();
        break;

      case PLATEAU:
        controller.playLongestPlateauGame();
        break;
    }
  }

  private boolean stop() {
    log.info("Do you wanna play again?");
    boolean stop = sc.nextBoolean();
    return stop;
  }

  private int readGame() {
    int game = sc.nextInt();
    while (game < FIRST_GAME || game > LAST_GAME) {
      log.info("Please enter correct game : ");
      game = sc.nextInt();
    }
    return game;
  }
}
