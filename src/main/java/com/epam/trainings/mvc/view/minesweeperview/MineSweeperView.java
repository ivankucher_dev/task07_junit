package com.epam.trainings.mvc.view.minesweeperview;

import com.epam.trainings.mvc.controller.minesweepercontroller.MineSweeperController;

public interface MineSweeperView {
  void paintResult(String toShow);

  boolean startPlay();

  int readInput();

  void setController(MineSweeperController controller);
}
