package com.epam.trainings.mvc.view.longestplateauview;

import com.epam.trainings.mvc.controller.longestplateaucontroller.LongestPlateauController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

public class LongestPlateauView {
  private LongestPlateauController controller;
  private static Logger log = LogManager.getLogger(LongestPlateauView.class.getName());
  private Scanner sc;
  private boolean firstStart;

  public LongestPlateauView() {
    sc = new Scanner(System.in);
    firstStart = true;
  }

  public void setController(LongestPlateauController controller) {
    this.controller = controller;
    startPlay();
  }

  public void showResult(String toShow) {
    log.info(toShow);
  }

  public void startPlay() {
    if (firstStart) {
      log.info("Welcome to Longest Plateau program");
      firstStart = false;
    }
    log.info("Generate array automatically?");
    boolean auto = readInputBoolean();
    controller.getLongestPlateau(controller.createArray(auto));
  }

  public int readInput() {
    return sc.nextInt();
  }

  public boolean readInputBoolean() {
    return sc.nextBoolean();
  }
}
