package com.epam.trainings.mvc.view.minesweeperview;

import com.epam.trainings.mvc.controller.minesweepercontroller.MineSweeperController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static com.epam.trainings.utils.PropertiesGetter.*;
import java.util.Scanner;

public class MineSweeperViewImpl implements MineSweeperView {
  private MineSweeperController controller;
  private Scanner sc;
  private boolean firstStart;
  private static Logger log = LogManager.getLogger(MineSweeperViewImpl.class.getName());

  public MineSweeperViewImpl() {
    sc = new Scanner(System.in);
    firstStart = true;
  }

  public void setController(MineSweeperController controller) {
    this.controller = controller;
    startPlay();
  }

  public void paintResult(String toShow) {
    System.out.println(toShow);
  }

  public boolean startPlay() {
    if (firstStart) {
      log.info("Welcome to Mine Sweeper game");
      firstStart = false;
    }
    log.info("Max rows and max columns = " + getIntProp("maxRows"));
    log.info("Probability from 1 to 99");
    int maxRows = getIntProp("maxRows");
    int maxColumns = getIntProp("maxColumns");
    int width;
    int height;
    int probability;
    int i = 0;
    do {
      if (i > 0) {
        log.warn("Enter data correctly : ");
      }
      System.out.println("Enter width");
      width = readInput();
      System.out.println("Enter height");
      height = readInput();
      System.out.println("Enter probability");
      probability = readInput();
      i++;
    } while (width > maxRows || height > maxColumns || (probability > 99 || probability < 1));
    controller.play(width, height, probability);
    return true;
  }

  public int readInput() {
    return sc.nextInt();
  }
}
