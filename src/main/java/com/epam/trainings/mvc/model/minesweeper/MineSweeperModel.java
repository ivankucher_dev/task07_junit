package com.epam.trainings.mvc.model.minesweeper;

import com.epam.trainings.mvc.model.minesweeper.entry.Entry;

public interface MineSweeperModel {
  void setupMineSweeperModel(int width, int height, int probability);

  boolean openField(int i, int j);

  Entry[][] getEntries();

  void setEntries(Entry[][] entries);

  String[][] getScreenVersion();

  int getSafe_fields_num();
}
