package com.epam.trainings.mvc.model.minesweeper;

import com.epam.trainings.mvc.model.minesweeper.entry.Entry;
import static com.epam.trainings.utils.PropertiesGetter.*;
import java.util.Random;

public class MineSweeper implements MineSweeperModel {
  private Entry[][] entries;
  private String[][] screenVersion;
  private int width;
  private int height;
  private int probability;
  private int safe_fields_num;
  private boolean inGame;
  private Random r;

  public MineSweeper() {
    inGame = true;
    r = new Random();
    generateEntries();
    generateScreenVersionOfGame();
  }

  public void setupMineSweeperModel(int width, int height, int probability) {
    this.width = width;
    this.height = height;
    this.probability = probability;
    generateEntries();
    generateScreenVersionOfGame();
  }

  public Entry[][] getEntries() {
    return entries;
  }

  public String[][] getScreenVersion() {
    return screenVersion;
  }

  public int getSafe_fields_num() {
    return safe_fields_num;
  }

  public boolean openField(int i, int j) {
    boolean openedBomb = entries[i][j].isBomb();
    entries[i][j].setOpened(true);
    if (openedBomb) {
      screenVersion[i][j] = getProperties().getProperty("bombView");
      inGame = false;
      return inGame;
    } else {
      safe_fields_num--;
      screenVersion[i][j] = "( " + entries[i][j].getNeighboringBombs() + " )";
      if (safe_fields_num == 0) {
        inGame = false;
      }
      return inGame;
    }
  }

  private void generateEntries() {
    entries = new Entry[height][width];
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        entries[i][j] = new Entry(getEntryType());
      }
    }
    setupNeighboringBombs();
  }

  private void setupNeighboringBombs() {
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        entries[i][j].setNeighboringBombs(countNeighboardingBombs(i, j));
      }
    }
  }

  public int countNeighboardingBombs(int i, int j) {
    if (!entries[i][j].isBomb()) {
      int tempMines = 0;
      if ((j - 1) != -1) {
        if (entries[i][j - 1].isBomb()) {
          tempMines += 1;
        }
      }
      if ((i - 1) != -1) {
        if (entries[i - 1][j].isBomb()) {
          tempMines += 1;
        }
      }
      if ((i - 1) != -1 && (j - 1) != -1) {
        if (entries[i - 1][j - 1].isBomb()) {
          tempMines += 1;
        }
      }
      if ((i - 1) != -1 && (j + 1) != height) {
        if (entries[i - 1][j + 1].isBomb()) {
          tempMines += 1;
        }
      }
      if ((i + 1) != width) {
        if (entries[i + 1][j].isBomb()) {
          tempMines += 1;
        }
      }
      if ((j + 1) != height) {
        if (entries[i][j + 1].isBomb()) {
          tempMines += 1;
        }
      }
      if ((i + 1) != width && (j + 1) != height) {
        if (entries[i + 1][j + 1].isBomb()) {
          tempMines += 1;
        }
      }
      if ((i + 1) != width && (j - 1) != -1) {
        if (entries[i + 1][j - 1].isBomb()) {
          tempMines += 1;
        }
      }
      entries[i][j].setNeighboringBombs(tempMines);
      return tempMines;
    }
    return 0;
  }

  private boolean getEntryType() {
    int low = 0;
    int high = 100;
    int result = r.nextInt(high - low) + low;
    if (result < probability) {
      return getBooleanProp("bomb");
    } else {
      safe_fields_num++;
      return getBooleanProp("safefield");
    }
  }

  private void generateScreenVersionOfGame() {
    screenVersion = new String[height][width];
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        screenVersion[i][j] = setDefaultFiledView(i, j);
      }
    }
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public void setEntries(Entry[][] entries) {
    this.entries = entries;
  }

  private String setDefaultFiledView(int i, int j) {
    return "[" + i + " " + j + "]";
  }
}
