package com.epam.trainings.mvc.model.lingestplateau;

public class Plateau {
  private int[] array;

  public void initArray(int[] array) {
    this.array = array;
  }

  public String longestPlateau() {
    if (array.length == 0) {
      throw new IllegalArgumentException();
    }
    int count = 1;
    int length = 1;
    for (int i = 1; i < array.length; i++) {
      if (array[i - 1] == array[i]) {
        count++;
        length = Math.max(length, count);
      } else {
        count = 1;
      }
    }
    StringBuilder sb = new StringBuilder();
    for (int e : array) {
      sb.append(e + ", ");
    }
    sb.append("\nLongest plateau = " + length);
    return String.valueOf(length);
  }
}
