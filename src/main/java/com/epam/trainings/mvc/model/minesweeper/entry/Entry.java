package com.epam.trainings.mvc.model.minesweeper.entry;

public class Entry {
  private boolean isBomb;
  private int neighboringBombs;
  private boolean isOpened;

  public Entry(boolean isBomb) {
    this.isBomb = isBomb;
    isOpened = false;
  }

  public boolean isOpened() {
    return isOpened;
  }

  public void setOpened(boolean opened) {
    isOpened = opened;
  }

  public boolean isBomb() {
    return isBomb;
  }

  public void setBomb(boolean bomb) {
    isBomb = bomb;
  }

  public int getNeighboringBombs() {
    return neighboringBombs;
  }

  public void setNeighboringBombs(int neighboringBombs) {
    this.neighboringBombs = neighboringBombs;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    if (isBomb) {
      sb.append("Bomb");
    } else {
      sb.append("Safe field");
    }
    sb.append(", neighboring bombs = " + neighboringBombs);
    return sb.toString();
  }
}
