package com.epam.trainings.main;

import com.epam.trainings.mvc.controller.viewcontroller.ViewController;
import com.epam.trainings.mvc.controller.viewcontroller.ViewControllerImpl;
import com.epam.trainings.mvc.view.mainview.View;

public class Main {
  public static void main(String[] args) {
    View view = new View();
    ViewController viewController = new ViewControllerImpl(view);
    view.setController(viewController);
  }
}
